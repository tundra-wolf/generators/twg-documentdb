use clap::ValueEnum;

/// Document databases
#[derive(Clone, Debug, PartialEq, ValueEnum)]
pub enum DocumentDatabase {
    CosmosDb,
    CouchDb,
    DynamoDb,
    MongoDb,
}

impl Default for DocumentDatabase {
    fn default() -> Self {
        Self::MongoDb
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn document_database() {
        let db = DocumentDatabase::CosmosDb;
        assert_eq!(DocumentDatabase::CosmosDb, db);
        let db = DocumentDatabase::CouchDb;
        assert_eq!(DocumentDatabase::CouchDb, db);
        let db = DocumentDatabase::DynamoDb;
        assert_eq!(DocumentDatabase::DynamoDb, db);
        let db = DocumentDatabase::MongoDb;
        assert_eq!(DocumentDatabase::MongoDb, db);

        let default_db = DocumentDatabase::default();
        assert_eq!(DocumentDatabase::MongoDb, default_db);
    }
}
